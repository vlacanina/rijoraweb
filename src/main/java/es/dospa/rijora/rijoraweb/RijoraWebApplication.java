package es.dospa.rijora.rijoraweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
public class RijoraWebApplication {

	@GetMapping("favicon.ico")
	public static void main(String[] args) {
		SpringApplication.run(RijoraWebApplication.class, args);
	}

}
